package com.gildedtros;

import com.gildedtros.facades.LegendaryItem;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * All items have a SellIn value which denotes the number of days we have to sell the item.
 *
 * <p>All items have a Quality value which denotes how valuable the item is.</p>
 *
 * <p>At the end of each day our system lowers both values for every item
 * <ul>
 *   <li>Once the sell by date has passed, Quality degrades twice as fast</li>
 *   <li>The Quality of an item is never negative</li>
 *   <li>"Good Wine" actually increases in Quality the older it gets</li>
 *   <li>The Quality of an item is never more than 50
 *     <ol>
 *       <li>Legendary items always have Quality 80</li>
 *     </ol>
 *   </li>
 *   <li>"B-DAWG Keychain", being a legendary item, never has to be sold or decreases in Quality</li>
 *   <li>
 *     "Backstage passes" for very interesting conferences increases in Quality as its SellIn value approaches;
 *     <ol>
 *       <li>Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less</li>
 *       <li>Quality drops to 0 after the conference</li>
 *     </ol>
 *   </li>
 *   <li>Smelly items ("Duplicate Code", "Long Methods", "Ugly Variable Names") degrade in Quality twice as fast as normal items</li>
 * </ul>
 * </p>
 */
class GildedTrosTest {

    /**
     * Verify that the basic set of rules are not violated:
     *
     * <p>At the end of each day our system lowers both values for every item
     * <ul>
     *   <li>Once the sell by date has passed, Quality degrades twice as fast</li>
     *   <li>The Quality of an item is never negative</li>
     *   <li>The Quality of an item is never more than 50</li>
     * </ul>
     * </p>
     */
    @Nested
    class TestBasicRules {

        /**
         * Verify that both "sell in" and "quality" decrease until they hit zero. Even if the number of days past is bigger than the "sill in".
         */
        @Test
        void testDecreaseUntilZero() {
            int amountOfSellInDays = 5;
            Item basicItem = new Item("Basic item", amountOfSellInDays, amountOfSellInDays);
            GildedTros gildedTros = new GildedTros(new Item[]{basicItem});

            final int amountOfDaysToPass = amountOfSellInDays * 2;
            int daysPast = 0;

            while (++ daysPast <= amountOfDaysToPass) {
                log("Number of days past: " + daysPast);
                gildedTros.updateQuality();
                log(basicItem.toString());

                int expectedResult = amountOfSellInDays - daysPast;
                assertEquals(expectedResult, basicItem.sellIn);
                assertEquals(Math.max(expectedResult, 0), basicItem.quality);
            }
        }

        /**
         * The quality of an item decreases twice as fast after the sell in.
         */
        @Test
        void testDecreaseAfterSellIn() {
            int amountOfSellInDays = 5;
            final int initialQuality = 20;

            Item basicItem = new Item("Basic item", 0, initialQuality);
            GildedTros gildedTros = new GildedTros(new Item[]{basicItem});

            int daysPast = 0;
            while (++ daysPast <= amountOfSellInDays) {
                log("Number of days past: " + daysPast);
                gildedTros.updateQuality();
                log(basicItem.toString());

                assertEquals(initialQuality - daysPast * 2, basicItem.quality);
            }
        }

        /**
         * Tests that the quality of an item never goes over its max, 50.
         */
        @Disabled("As the 'Item' cannot be touched, this will always fail. Checks are provided tho, see UpdatableItem#verifyAndAdjustItem.")
        @Test
        void testMaxAmountOfQuality() {
            IntStream.rangeClosed(0, 51)
                .mapToObj(counter -> new Item("Item: " + counter, counter, counter))
                .peek(item -> log(item.toString()))
                .forEach(item ->
                    assertTrue(
                        item.quality <= 50,
                        "The quality was '" + item.quality + "', max is '50'"));
        }

        /**
         * Test that "legendary items" always have Quality 80.
         */
        @Test
        void testQualityLegendaryItem() {
            int initialValue = 5;
            Item legendaryItem = new Item("B-DAWG Keychain", initialValue, initialValue);
            GildedTros gildedTros = new GildedTros(new Item[]{legendaryItem});

            int daysPast = 0;
            while (++ daysPast <= initialValue) {
                log("Number of days past: " + daysPast);
                gildedTros.updateQuality();
                log(legendaryItem.toString());

                assertEquals(initialValue, legendaryItem.sellIn);
                assertEquals(LegendaryItem.FIXED_QUALITY, legendaryItem.quality);
            }
        }
    }

    /**
     * Validate the set of rules for "Good Wine":
     * <ul>
     *   <li>"Good Wine" actually increases in Quality the older it gets</li>
     *   <li>Once the sell by date has passed, Quality degrades twice as fast => <b>it increases twice as fast</b></li>
     * </ul>
     */
    @Nested
    class TestGoodWine {

        /**
         * The quality of "Good Wine" should increase, not decrease.
         *
         * @implNote The "sell in" in this test shouldn't go negative. This is another rule of the item and is tested in {@link #testDoubleQualityIncreaseAfterSellIn()}
         */
        @Test
        void testIncreasesQualityWhenAging() {
            int amountOfSellInDays = 5;
            Item wine = new Item("Good Wine", amountOfSellInDays, amountOfSellInDays);
            GildedTros gildedTros = new GildedTros(new Item[]{wine});

            int daysPast = 0;
            while (++ daysPast <= amountOfSellInDays) {
                log("Number of days past: " + daysPast);
                gildedTros.updateQuality();
                log(wine.toString());

                assertEquals(amountOfSellInDays + daysPast, wine.quality);
            }
        }

        /**
         * Only tests that the quality increases twice as fast after the "sell in".
         */
        @Test
        void testDoubleQualityIncreaseAfterSellIn() {
            int amountOfSellInDays = 5;
            Item wine = new Item("Good Wine", 0, amountOfSellInDays);
            GildedTros gildedTros = new GildedTros(new Item[]{wine});

            int daysPast = 0;
            while (++ daysPast <= amountOfSellInDays) {
                log("Number of days after sell in: " + daysPast);
                gildedTros.updateQuality();
                log(wine.toString());

                assertEquals(amountOfSellInDays + daysPast * 2, wine.quality);
            }
        }
    }

    /**
     * Check the rules for the item "B-DAWG Keychain"
     */
    @Nested
    class TestBDAWG {

        /**
         * Never has to be sold or decreases in Quality.
         */
        @Test
        void testNeverDecreasesValues() {
            int initialValue = 5;
            Item keychain = new Item("B-DAWG Keychain", initialValue, initialValue);
            GildedTros gildedTros = new GildedTros(new Item[]{keychain});

            int daysPast = 0;
            while (++ daysPast <= initialValue) {
                log("Number of days past: " + daysPast);
                gildedTros.updateQuality();
                log(keychain.toString());

                assertEquals(80, keychain.quality);
            }
        }
    }

    /**
     * Verify the "Backstage passes" rules.
     *
     * <ul>
     *   <li>
     *     Increases in Quality as its SellIn value approaches
     *     <ol>
     *       <li>Quality increases by 2 when there are 10 days or less</li>
     *       <li>Quality increases by 3 when there are 5 days or less</li>
     *     </ol>
     *   </li>
     *   <li>Quality drops to 0 after the conference</li>
     * </ul>
     */
    @Nested
    class TestBackstagePasses {

        /**
         * Validate that the quality increases as the days pass
         *
         * @implNote Doesn't take the "special conditions" into account.
         * @see #testTenDaysTillSellIn()
         * @see #testFiveDaysTillSellIn()
         */
        @Test
        void testIncreaseQuality() {
            countDownFiveDaysAndAssertQuality(5, 20, 1);
        }

        /**
         * Check that the quality increases by two ten days till sell in.
         *
         * @see #testIncreaseQuality()
         * @see #testFiveDaysTillSellIn()
         */
        @Test
        void testTenDaysTillSellIn() {
            countDownFiveDaysAndAssertQuality(5, 10, 2);
        }
        /**
         * Check that the quality increases by three five days till sell in. day.
         *
         * @see #testIncreaseQuality()
         * @see #testTenDaysTillSellIn()
         */
        @Test
        void testFiveDaysTillSellIn() {
            countDownFiveDaysAndAssertQuality(5, 5, 3);
        }

        // Note could also be a @ParameterizedTest instead of a private method.
        /**
         * Given the item "Backstage passes for Re:Factor". Count down five days and then assert that the quality is increased by the amount of days past times the given factor.
         *
         * @param initialQuality The initial quality of the item
         * @param initialSellIn The initial sell in of the item
         * @param factorDaysPast The factor applied to the "days past"
         */
        private void countDownFiveDaysAndAssertQuality(
            int initialQuality,
            int initialSellIn,
            int factorDaysPast
        ) {
            Item backstagePass = new Item("Backstage passes for Re:Factor", initialSellIn, initialQuality);
            GildedTros gildedTros = new GildedTros(new Item[]{backstagePass});

            int amountOfSellInDays = 5;
            int daysPast = 0;

            while (++ daysPast <= amountOfSellInDays) {
                log("Number of days past: " + daysPast);
                gildedTros.updateQuality();
                log(backstagePass.toString());

                assertEquals(initialQuality + daysPast * factorDaysPast, backstagePass.quality);
            }
        }

        @Test
        void testQualityAfterSellIn() {
            Item backstagePass = new Item("Backstage passes for Re:Factor", 0, 50);
            GildedTros gildedTros = new GildedTros(new Item[]{backstagePass});

            gildedTros.updateQuality();
            log(backstagePass.toString());

            assertEquals(0, backstagePass.quality);
        }
    }

    /**
     * 	Smelly items ("Duplicate Code", "Long Methods", "Ugly Variable Names") degrade in Quality twice as fast as normal items
     */
    @Nested
    class TestSmellyItems {

        /**
         * Smelly items should drop their quality twice as fast.
         */
        @Test
        void testDegradeQuality() {
            int initialValue = 10;
            Item duplicateCode = new Item("Duplicate Code", initialValue, initialValue);
            GildedTros gildedTros = new GildedTros(new Item[]{duplicateCode});

            int daysPast = 0;

            while (++ daysPast <= initialValue) {
                log("Number of days past: " + daysPast);
                gildedTros.updateQuality();
                log(duplicateCode.toString());

                assertEquals(initialValue - daysPast * 2, duplicateCode.quality);
            }
        }
    }

    /**
     * Print the provided message as an info statement.
     *
     * <p>Would prefer to use an actual logger, but that's not the point of this repo ;)</p>
     *
     * @param message The message to log.
     */
    private void log(String message) {
        System.out.println(message);
    }

}
