package com.gildedtros.facades;

import com.gildedtros.Item;

/**
 * Some sort of item of legend. It's quality never decreases and will always have "80".
 *
 * <p>When implementing this class, the following functions {@link UpdatableItem#verifyAndAdjustItem()} and {@link UpdatableItem#verifyAndAdjustItem(Item)} can't be overridden.</p>
 */
public class LegendaryItem implements UpdatableItem {

  /**
   * The quality of a legendary item should always be fixed to this value.
   */
  public static final int FIXED_QUALITY = 80;

  protected final Item backReference;

  protected LegendaryItem(Item backReference) {
    this.backReference = backReference;
  }

  @Override
  public final void updateQuality() {
    // Does nothing
  }

  @Override
  public final void verifyAndAdjustItem() {
    verifyAndAdjustItem(backReference);
  }

  @Override
  public final void verifyAndAdjustItem(Item item) {
    if (item.quality != FIXED_QUALITY) {
      item.quality = FIXED_QUALITY;
    }
  }

  @Override
  public String toString() {
    return backReference.toString();
  }
}
