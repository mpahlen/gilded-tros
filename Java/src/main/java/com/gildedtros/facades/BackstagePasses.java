package com.gildedtros.facades;

import com.gildedtros.Item;

import java.util.Arrays;

/**
 * Represents backstage passes for certain conferences.
 *
 * <p>These items increases in quality as its sell in approaches</p>
 */
public class BackstagePasses implements UpdatableItem {

  /**
   * Lists the possible conferences the {@link BackstagePasses} can be used for.
   */
  public enum Conferences {
    HAXX("HAXX"),
    RE_FACTOR("Re:Factor");

    private final String name;

    Conferences(String name) {
      this.name = name;
    }
  }

  private static final String NAME_PREFIX = "Backstage passes for ";

  /**
   * Tests if the {@link Item} is a candidate to be {@link BackstagePasses}.
   *
   * @param item The {@link BackstagePasses} candidate.
   * @return <code>true</code> if the item can be wrapped, <code>false</code> otherwise.
   */
  static boolean matches(Item item) {
    return Arrays.stream(Conferences.values())
        .anyMatch(conference -> (NAME_PREFIX + conference.name).equals(item.name));
  }

  /**
   * Wraps the item into a {@link BackstagePasses}, if possible.
   *
   * @param item The item to wrap.
   * @return An instance of {@link BackstagePasses}, or <code>null</code> if the item can't be wrapped.
   */
  static BackstagePasses wrapIfPossible(Item item) {
    if (matches(item)) {
      return new BackstagePasses(item);
    }

    return null;
  }

  private final Item backReference;

  BackstagePasses(Item item) {
    this.backReference = item;
  }

  @Override
  public void updateQuality() {
    backReference.sellIn --;

    if (backReference.sellIn < 0) {
      backReference.quality = 0;
    } else if (backReference.sellIn < 5) {
      backReference.quality += 3;
    } else if (backReference.sellIn < 10) {
      backReference.quality += 2;
    } else {
      backReference.quality ++;
    }
  }

  @Override
  public void verifyAndAdjustItem() {
    verifyAndAdjustItem(backReference);
  }

  @Override
  public String toString() {
    return backReference.toString();
  }
}
