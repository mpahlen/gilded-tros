package com.gildedtros.facades;

import com.gildedtros.Item;

/**
 * Represents an item named "B-DAWG Keychain". This item never has to be sold or decreases in quality.
 */
public class BDawgKeychain
    extends LegendaryItem
    implements UpdatableItem {

  private static final String NAME = "B-DAWG Keychain";

  /**
   * Tests if the {@link Item} is a candidate to be {@link GoodWine}.
   *
   * @param item The {@link GoodWine} candidate.
   * @return <code>true</code> if the item can be wrapped, <code>false</code> otherwise.
   */
  static boolean matches(Item item) {
    return NAME.equals(item.name);
  }

  /**
   * Wraps the item into a {@link BDawgKeychain}, if possible.
   *
   * @param item The item to wrap.
   * @return An instance of {@link BDawgKeychain}, or <code>null</code> if the item can't be wrapped.
   */
  static BDawgKeychain wrapIfPossible(Item item) {
    if (matches(item)) {
      return new BDawgKeychain(item);
    }

    return null;
  }

  BDawgKeychain(Item backReference) {
    super(backReference);
  }

}
