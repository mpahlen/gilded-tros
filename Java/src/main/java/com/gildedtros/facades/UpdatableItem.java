package com.gildedtros.facades;

import com.gildedtros.Item;

/**
 * This {@link UpdatableItem} allows items to control their behaviour and be responsible for the update of their properties. This is achieved through the {@link UpdatableItem#updateQuality()}.
 *
 * @see ItemFacades
 */
public interface UpdatableItem {

  /**
   * Update the quality of the represented item.
   */
  void updateQuality();

  /**
   * Verifies and corrects this item if needed.
   *
   * @see #verifyAndAdjustItem(Item)
   */
  void verifyAndAdjustItem();

  /**
   * Verifies and corrects the item if needed.
   *
   * <p>By default the "quality" of an item cannot be over 50.</p>
   *
   * @param item The item to verify.
   */
  default void verifyAndAdjustItem(Item item) {
    if (item.quality > 50) {
      item.quality = 50;
    }
  }

}
