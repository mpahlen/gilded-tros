package com.gildedtros.facades;

import com.gildedtros.Item;

import java.util.Arrays;

/**
 * Represents an {@link Item} that you don't want: bad practices!
 *
 * <p>These items decrease their quality twice as fast!</p>
 */
public class SmellyItem implements UpdatableItem {

  /**
   * All possible sorts of {@link SmellyItem}s.
   */
  public enum Types {
    DUPLICATE_CODE("Duplicate Code"),
    LONG_METHODS("Long Methods"),
    UGLY_VARIABLE_NAMES("Ugly Variable Names");

    private final String typeName;

    Types(String typeName) {
      this.typeName = typeName;
    }
  }

  /**
   * Tests if the {@link Item} is a candidate to be {@link SmellyItem}.
   *
   * @param item The {@link SmellyItem} candidate.
   * @return <code>true</code> if the item can be wrapped, <code>false</code> otherwise.
   */
  static boolean matches(Item item) {
    return Arrays.stream(Types.values())
        .map(type -> type.typeName)
        .anyMatch(typeName -> typeName.equals(item.name));
  }

  /**
   * Wraps the item into a {@link SmellyItem}, if possible.
   *
   * @param item The item to wrap.
   * @return An instance of {@link SmellyItem}, or <code>null</code> if the item can't be wrapped.
   */
  static SmellyItem wrapIfPossible(Item item) {
    if (matches(item)) {
      return new SmellyItem(item);
    }

    return null;
  }

  private final Item backReference;

  SmellyItem(Item item) {
    this.backReference = item;
  }

  @Override
  public void updateQuality() {
    backReference.sellIn --;
    backReference.quality -= 2;
  }

  @Override
  public void verifyAndAdjustItem() {
    verifyAndAdjustItem(backReference);
  }
}
