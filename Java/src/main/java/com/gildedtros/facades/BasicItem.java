package com.gildedtros.facades;

import com.gildedtros.Item;

/**
 * A normal item who's quality decreases when reaching the "sell in" day.
 */
public class BasicItem implements UpdatableItem {

  private final Item item;

  BasicItem(Item item) {
    this.item = item;
  }

  @Override
  public void updateQuality() {
    item.sellIn --;

    if (item.quality > 0) {
      if (item.sellIn < 0) {
        item.quality -= 2;
      } else {
        item.quality --;
      }
    }
  }

  @Override
  public void verifyAndAdjustItem() {
    verifyAndAdjustItem(item);
  }

  @Override
  public String toString() {
    return item.toString();
  }
}
