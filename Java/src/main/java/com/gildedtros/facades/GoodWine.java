package com.gildedtros.facades;

import com.gildedtros.Item;

/**
 * Represents an item named "Good Wine". This item improves when aging.
 */
public class GoodWine implements UpdatableItem {

  private static final String NAME = "Good Wine";

  /**
   * Tests if the {@link Item} is a candidate to be {@link GoodWine}.
   *
   * @param item The {@link GoodWine} candidate.
   * @return <code>true</code> if the item can be wrapped, <code>false</code> otherwise.
   */
  static boolean matches(Item item) {
    return NAME.equals(item.name);
  }

  /**
   * Wraps the item into a {@link GoodWine}, if possible.
   *
   * @param item The item to wrap.
   * @return An instance of {@link GoodWine}, or <code>null</code> if the item can't be wrapped.
   */
  static GoodWine wrapIfPossible(Item item) {
    if (matches(item)) {
      return new GoodWine(item);
    }

    return null;
  }

  private final Item backReference;

  GoodWine(Item backReference) {
    this.backReference = backReference;
  }

  /** {@inheritDoc */
  @Override
  public void updateQuality() {
    backReference.sellIn --;

    if (backReference.sellIn >= 0) {
      backReference.quality ++;
    } else {
      backReference.quality += 2;
    }
  }

  @Override
  public void verifyAndAdjustItem() {
    verifyAndAdjustItem(backReference);
  }

  @Override
  public String toString() {
    return backReference.toString();
  }
}
