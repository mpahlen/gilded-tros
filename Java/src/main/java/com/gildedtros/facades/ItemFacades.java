package com.gildedtros.facades;

import com.gildedtros.Item;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * A utility class that creates facades for an {@link Item}. These "facades" are implementations of the {@link UpdatableItem} interface.
 *
 * <p>This {@link UpdatableItem} allows items to control their behaviour and be responsible for the update of their properties. This is achieved through the {@link UpdatableItem#updateQuality()}.</p>
 *
 * <p>An item should always be convertable to an item, the fallback is the {@link BasicItem}.</p>
 */
public class ItemFacades {

  /**
   * This map contains functions that can be used to "wrap" an {@link Item} into an {@link UpdatableItem}.
   *
   * <p>It's key is a {@link Predicate} which determines if the item can be wrapped into the represented implementation. The value is the actual mapper function.</p>
   */
  private static final Map<Predicate<Item>, Function<Item, UpdatableItem>> ITEM_WRAPPERS = new HashMap<>();

  static {
    ITEM_WRAPPERS.put(GoodWine::matches, GoodWine::wrapIfPossible);
    ITEM_WRAPPERS.put(BDawgKeychain::matches, BDawgKeychain::wrapIfPossible);
    ITEM_WRAPPERS.put(BackstagePasses::matches, BackstagePasses::wrapIfPossible);
    ITEM_WRAPPERS.put(SmellyItem::matches, SmellyItem::wrapIfPossible);
  }

  /**
   * Factory method for creating an {@link ItemFacades} based on an array of {@link Item}s.
   *
   * @param items The items that should be wrapped.
   * @return A new {@link ItemFacades} instance.
   */
  public static ItemFacades forItems(Item... items) {
    return new ItemFacades(Arrays.asList(items));
  }

  private final Collection<Item> items;

  private ItemFacades(Collection<Item> items) {
    this.items = items;
  }

  /**
   * @return Stream the items as {@link UpdatableItem}.
   */
  public Stream<UpdatableItem> stream() {
    return items.stream().map(this::wrapSingleItem);
  }

  private UpdatableItem wrapSingleItem(Item item) {
    Optional<Predicate<Item>> matchingPredicate =
        ITEM_WRAPPERS
            .keySet()
            .stream()
            .filter(itemPredicate -> itemPredicate.test(item))
            .findFirst();

    if (!matchingPredicate.isPresent()) {
      return new BasicItem(item);
    }

    UpdatableItem updatableItem = ITEM_WRAPPERS
        .get(matchingPredicate.get())
        .apply(item);

    if (updatableItem != null) {
      return updatableItem;
    }

    // Should never be the case, but since the method can return null,
    // better be safe than sorry.
    return new BasicItem(item);
  }
}
