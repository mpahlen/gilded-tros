package com.gildedtros;

import com.gildedtros.facades.ItemFacades;
import com.gildedtros.facades.UpdatableItem;

import java.util.function.Consumer;

class GildedTros {
    Item[] items;

    public GildedTros(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        final Consumer<UpdatableItem> updateQuality = UpdatableItem::updateQuality;
        final Consumer<UpdatableItem> verifyAndAdjust = UpdatableItem::verifyAndAdjustItem;

        ItemFacades
            .forItems(this.items)
            .stream()
            .forEach(updateQuality.andThen(verifyAndAdjust));
    }

}